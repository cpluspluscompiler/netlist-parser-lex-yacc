/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUMBER = 258,
    IDENTITY = 259,
    ADD = 260,
    SUB = 261,
    ASSIGN = 262,
    NLINE = 263,
    MUL = 264,
    DIV = 265,
    REMA = 266,
    NADD = 267,
    NSUB = 268,
    NMUL = 269,
    NDIV = 270,
    COS = 271,
    SIN = 272,
    LOG = 273,
    NEG = 274,
    ABS = 275,
    INCRE = 276,
    DECRE = 277,
    POW = 278
  };
#endif
/* Tokens.  */
#define NUMBER 258
#define IDENTITY 259
#define ADD 260
#define SUB 261
#define ASSIGN 262
#define NLINE 263
#define MUL 264
#define DIV 265
#define REMA 266
#define NADD 267
#define NSUB 268
#define NMUL 269
#define NDIV 270
#define COS 271
#define SIN 272
#define LOG 273
#define NEG 274
#define ABS 275
#define INCRE 276
#define DECRE 277
#define POW 278

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 20 "Compileryacc.y" /* yacc.c:1909  */
 
    int id;
    double num; 

#line 105 "y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
