%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
extern int yylex();
void yyerror(char *);
int vararray[10];
int linenum = 1;
double get;
double getvar(int var);
void savevar(int var , double n); 
double getpow(double b, int n);
int getlog(double n);
double getneg(double n);
int getabs(double n);
double getincre(double n);
double getdecre(double n);
double getresidue(double divisor, double dividend);
%}
%union { 
    int id;
    double num; 
}
%token <num> NUMBER
%token <id> IDENTITY
%type <num> lines expression term factor group
%nonassoc ADD SUB ASSIGN NLINE
%nonassoc MUL DIV REMA NADD NSUB NMUL NDIV
%left COS SIN LOG NEG ABS
%nonassoc INCRE DECRE POW
%%
lines   : 
        | lines IDENTITY ASSIGN expression NLINE {
                                                    if($4 != 0.0)
                                                    {
                                                        savevar($2, $4); 
                                                        if($4 - (int)$4 == 0)
                                                            printf("%d\n" ,(int)$4);
                                                        else
                                                            printf("%lf\n", $4);
                                                        linenum++;
                                                    }
                                                    else
                                                    {
                                                        printf("Line%d : var%d is undefined\n",linenum, (int)get);
                                                        linenum++;
                                                    }
                                                }
        | lines IDENTITY ASSIGN expression NADD expression NLINE { printf("Line%d : syntex error with token + \n", linenum);linenum++; }
        | lines IDENTITY ASSIGN expression NSUB expression NLINE { printf("Line%d : syntex error with token - \n", linenum);linenum++; }
        | lines IDENTITY ASSIGN expression NMUL expression NLINE { printf("Line%d : syntex error with token * \n", linenum);linenum++; }
        | lines IDENTITY ASSIGN expression NDIV expression NLINE { printf("Line%d : syntex error with token / \n", linenum);linenum++; }
        ;
expression  : term { $$ = $1; }
            | expression ADD term { $$ = $1 + $3;  }
            | expression SUB term { $$ = $1 - $3; }
            ;
term    : factor { $$ = $1; }
        | term MUL factor { $$ = $1 * $3; }
        | term DIV factor { $$ = $1 / $3; }
        | term REMA factor { $$ = getresidue($1,$3); }
        | term POW factor { $$ = getpow($1, $3); }
        ;
factor  : NUMBER { $$ = $1; }
        | IDENTITY { $$ = getvar($1);}
        | COS factor { $$ = cos((int)$2); } 
        | SIN factor { $$ = sin((int)$2); }
        | LOG factor { $$ = getlog($2); }
        | NEG factor { $$ = getneg($2); } 
        | ABS factor { $$ = getabs($2); }
        | INCRE factor { $$ = getincre($2); }
        | factor INCRE { $$ = getincre($1); }
        | DECRE factor { $$ = getdecre($2); }
        | factor DECRE { $$ = getdecre($1); }
        | group { $$ = $1; }
        ;
group   : '(' expression ')' { $$ = $2; }
        ;
%%
void yyerror(char *s) 
{    
    printf("a %s\n", s);
}
double getpow(double b, int n)
{
    double r = 1; 
    for(int i = 0;i < n; i++)
    {
        r = r * b;
    }
    return r;
}
int getlog(double n)
{
    int num = (int)n;
    int r = 0;
    while(num > 0)
    {
        num = num/10;
        r++;
    }
    return r - 1;
}
double getneg(double n)
{
    return n - (n*2);
}
int getabs(double n)
{
    if(n < 0)
        return getneg(n); 
    else
        return n;
}
double getincre(double n)
{
    return n+1;
}
double getdecre(double n)
{
    return n-1; 
}
double getresidue(double divisor, double dividend)
{
    return divisor - ((divisor/dividend)*dividend);
}
double getvar(int var)
{
    if(vararray[var] == 0)
        get = var;
    return vararray[var];
}
void savevar(int var, double n)
{
	vararray[var] = n;
}
int main(void)
{
    for(int i=0;i<10;i++)
    {
        vararray[i] = 0;
    }
    yyparse();
    return 0;
}